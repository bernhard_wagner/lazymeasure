import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {DeviceConnectedGuard} from './bluetooth/device-connected-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'bluetooth', pathMatch: 'full' },
  { path: 'bluetooth', loadChildren: './bluetooth/bluetooth.module#BluetoothPageModule' },
  { path: 'measure', loadChildren: './measure/measure.module#MeasurePageModule', canActivate: [DeviceConnectedGuard] },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
