import { Injectable } from '@angular/core';
import {AlertController, ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
      private alertController: AlertController,
      private toastController: ToastController
  ) { }

  showToast(msg) {
    this.toastController.create({
      message: msg,
      duration: 1000
    }).then(toast => toast.present());
  }

  showError(title: string, text: string) {
    this.alertController.create({
      header: title,
      message: text,
      buttons: ['Dismiss']
    }).then(alert => alert.present());
  }
}
