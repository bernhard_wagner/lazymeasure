import {Injectable, NgZone} from '@angular/core';
import {Events} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class RenderWorkaroundService {

  constructor(
      public events: Events,
      private zone: NgZone
  ) {
    this.initNgZoneWorkaround();
  }

  private initNgZoneWorkaround() {
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
      });
    });
  }

  forceRender() {
    this.events.publish('updateScreen');
  }
}
