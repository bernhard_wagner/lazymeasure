import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {BluetoothService} from './bluetooth.service';

@Injectable({
        providedIn: 'root'
    })
export class DeviceConnectedGuard implements CanActivate {
    constructor(public bluetoothService: BluetoothService, public router: Router) {}
    canActivate(): boolean {
        if (!this.bluetoothService.hasConnectedDevice()) {
            this.router.navigate(['bluetooth']);
            return false;
        }

        return  true;
    }
}
