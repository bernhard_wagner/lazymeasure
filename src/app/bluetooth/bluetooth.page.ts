import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {BluetoothService} from './bluetooth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Events, NavController} from '@ionic/angular';
import {RenderWorkaroundService} from '../shared/render-workaround.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bluetooth',
  templateUrl: './bluetooth.page.html',
  styleUrls: ['./bluetooth.page.scss'],
})
export class BluetoothPage implements OnInit, OnDestroy {
  private $onDestroy: Subject<boolean> = new Subject<boolean>();
  devices: any;
  loading = false;

  constructor(
      private bluetoothService: BluetoothService,
      public navController: NavController,
      private renderWorkaround: RenderWorkaroundService,
      private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;
    this.bluetoothService.checkIfBluetoothEnabled(true);

    this.bluetoothService.$isLoadingDevices.pipe(takeUntil(this.$onDestroy)).subscribe(loading => {
      this.loading = loading;
      this.renderWorkaround.forceRender();
    });

    this.bluetoothService.$availableDevices.pipe(takeUntil(this.$onDestroy)).subscribe(devices => {
      this.devices = devices;
      this.renderWorkaround.forceRender();
    });

    this.bluetoothService.$connectedDevice.pipe(takeUntil(this.$onDestroy)).subscribe(this.goToMeasure.bind(this));
  }

  ngOnDestroy() {
    this.$onDestroy.next(true);
    this.$onDestroy.unsubscribe();
    this.bluetoothService.disconnectDevice();
  }

  discoverDevices() {
    this.bluetoothService.discoverDevices();
  }

  onSelectDevice(device) {
    this.bluetoothService.connectDevice(device);
  }

  private goToMeasure(value: any) {
    this.router.navigate(['measure']);
  }

}
