import { Injectable } from '@angular/core';
import {BluetoothSerial} from '@ionic-native/bluetooth-serial/ngx';
import {BehaviorSubject} from 'rxjs';
import {ToastService} from '../shared/toast.service';
import {Measurement} from '../measure/measure.model';
import {MeasureService} from '../measure/measure.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BluetoothService {
  private $availableDevicesSubject = new BehaviorSubject([]);
  private $isLoadingDevicesSubject = new BehaviorSubject(false);
  private $connectedDeviceSubject = new BehaviorSubject(null);
  public readonly $availableDevices  = this.$availableDevicesSubject.asObservable();
  public readonly $isLoadingDevices  = this.$isLoadingDevicesSubject.asObservable();
  public readonly $connectedDevice = this.$connectedDeviceSubject.asObservable();

  constructor(
      private bluetoothSerial: BluetoothSerial,
      private toastService: ToastService,
      private measureService: MeasureService,
      private router: Router
  ) {
    this.onInit();
  }

  private onInit() {
    this.initDeviceListListener();
    this.checkIfBluetoothEnabled(true);
  }

  private initDeviceListListener() {
    this.bluetoothSerial.setDeviceDiscoveredListener()
        .subscribe((data: PairedList) => {
          const availableDevices = [...this.$availableDevicesSubject.getValue(), data]
              .reduce((devices, current) => {
                if (!!current.name && !devices.find(d => d.address === current.address)) {
                  devices.push(current);
                }
                return devices;
          }, []);
          this.$availableDevicesSubject.next(availableDevices);
        });
  }

  checkIfBluetoothEnabled(force?: boolean) {
    this.bluetoothSerial.isEnabled().then(success => {
      setTimeout(() => this.discoverDevices(), 800);
    }, error => {
      if (force) {
        this.bluetoothSerial.enable().then(() => {
          this.checkIfBluetoothEnabled();
        });
      } else {
        this.toastService.showError('Bluetooth Error', 'Please Enable Bluetooth on your device');
        this.$isLoadingDevicesSubject.next(false);
      }

    });
  }

  discoverDevices() {
    this.$availableDevicesSubject.next([]);

    this.$isLoadingDevicesSubject.next(true);
    this.bluetoothSerial.discoverUnpaired().then((data) => {
      this.$isLoadingDevicesSubject.next(false);
    });
  }

  connectDevice(device) {
    this.bluetoothSerial.connect(device.address).subscribe(success => {
      this.toastService.showToast('Connected Sucessfully');
      this.$connectedDeviceSubject.next(device);
      this.recieveData();
    }, error => this.toastService.showError('Error', error));
  }

  disconnectDevice() {
    this.bluetoothSerial.disconnect().then(() => {
      this.toastService.showToast('Device Disconnected');
      this.$connectedDeviceSubject.next(null);
    });
  }

  hasConnectedDevice() {
    return !!this.$connectedDeviceSubject.getValue();
  }

  private recieveData() {
    this.bluetoothSerial.subscribe(';').subscribe(data => {
        const measurement = this.parseSerialString(data);
        this.measureService.update(measurement);
    }, error => {
      this.toastService.showError('Error', error);
      this.disconnectDevice();
    });
  }

  private parseSerialString(data: string) {
    //console.log(data);
    const result = new Measurement(0, 0);
    data.split('&').forEach(valStr => {
        if (valStr.includes('waist')) {
          result.waist = parseFloat(valStr.replace('waist=', ''));
          return;
        }
        if (valStr.includes('shoulder')) {
          result.shoulder = parseFloat(valStr.replace('shoulder=', ''));
          return;
        }
      });
    return result;
  }

}
