export class Measurement {
    constructor(
        public waist: number,
        public shoulder: number
    ) {}
}

export class MeasurementBuffer {
    constructor(
        public bufferSize: number,
        public waist: number[] = [],
        public shoulder: number[] = []
    ) {}
}
