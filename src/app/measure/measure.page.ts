import {Component, OnDestroy, OnInit} from '@angular/core';
import {MeasureService} from './measure.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Measurement} from './measure.model';
import {BluetoothService} from '../bluetooth/bluetooth.service';
import {RenderWorkaroundService} from '../shared/render-workaround.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.page.html',
  styleUrls: ['./measure.page.scss'],
})
export class MeasurePage implements OnInit, OnDestroy {
  private $onDestroy: Subject<boolean> = new Subject<boolean>();
  measurement: Measurement;

  constructor(
      private measureService: MeasureService,
      private bluetoothService: BluetoothService,
      private renderWorkaround: RenderWorkaroundService,
      private router: Router
  ) { }

  ngOnInit() {
    this.measureService.$measure.pipe(takeUntil(this.$onDestroy)).subscribe(measure => {
      if (measure == null) {
        this.router.navigate(['bluetooth']);
      }
      this.measurement = measure;
      this.renderWorkaround.forceRender();
    });
  }

  ngOnDestroy() {
    this.$onDestroy.next(true);
    this.$onDestroy.unsubscribe();
    this.bluetoothService.disconnectDevice();
  }

}
