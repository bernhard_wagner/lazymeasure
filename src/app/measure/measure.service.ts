import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Measurement, MeasurementBuffer} from './measure.model';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {
  private measureBuffer = new MeasurementBuffer(10); // Smoothes the values out (takes moving median)
  private $measureSubject = new BehaviorSubject(new Measurement(0, 0));
  public readonly $measure = this.$measureSubject.asObservable();

  constructor() { }

  update(measure: Measurement) {
    const medianMeasure = new Measurement(0, 0);
    const medianIndex = Math.floor(this.measureBuffer.bufferSize / 2);
    this.measureBuffer.waist = [measure.waist, ...this.measureBuffer.waist];
    this.measureBuffer.shoulder = [measure.waist, ...this.measureBuffer.shoulder];

    if (this.measureBuffer.waist.length > this.measureBuffer.bufferSize) {
      this.measureBuffer.waist.pop();
      this.measureBuffer.shoulder.pop();

      // todo for more exact values calculate average of more than one median value
      medianMeasure.waist = [...this.measureBuffer.waist].sort(((a, b) => a - b))[medianIndex];
      medianMeasure.shoulder = [...this.measureBuffer.shoulder].sort(((a, b) => a - b))[medianIndex];

      this.$measureSubject.next(medianMeasure);
    }
  }
}
